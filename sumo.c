#define SENSOR_ULTRASOM IN_1
#define SENSOR_COR IN_2
#define SENSOR_TOQUE IN_4
#define M_ESQ OUT_B
#define M_DIR OUT_C
#define MOTRIZES OUT_BC
#define M_PA OUT_A
#define GATILHO_DISTANCIA 48
#define GATILHO_LUZ 48
#define POTENCIA_MOTOR 85

enum possiveis_modos {
  sobrevivendo,
  buscando,
  atacando
} modo;

enum sentido {
     frente,
     tras
};

// parametros motor
struct pMotor {
  int angulo;
  int potencia;
  sentido sentido;
  bool movendo;
};
pMotor esq, dir;

mutex movMutex;

// protótipos das funções
float calculaMediaLeitura(byte valores[], int tamanho);
sub giraEsq(int angulo, int potencia);
sub giraDir(int angulo, int potencia);
task movMotorEsq();
task movMotorDir();
task moveRobo();
task ataca();
task enxerga();
task busca();
task sobrevive();
task equilibra();

task main() {

  // config sensor ultrasom
  SetSensorLowspeed(SENSOR_ULTRASOM);
  //SetSensorType(SENSOR_ULTRASOM, SENSOR_LOWSPEED);
  //SetSensorMode(SENSOR_ULTRASOM, SENSOR_MODE_RAW);
  //ResetSensor(SENSOR_ULTRASOM);

  // config sensor cor
  //SetSensorType(SENSOR_COR, IN_TYPE_LIGHT_ACTIVE);
  //SetSensorMode(SENSOR_COR, IN_MODE_RAW);
  //ResetSensor(SENSOR_COR);
  SetSensorLight(SENSOR_COR);

  // config sensor toque
  // SetSensorType(SENSOR_TOQUE, SENSOR_TOUCH); 
  // SetSensorMode(SENSOR_TOQUE, SENSOR_MODE_RAW);
  // ResetSensor(SENSOR_TOQUE);
  SetSensor(SENSOR_TOQUE, SENSOR_TOUCH);

  // espera de 5s + 250ms para n?o queimar largada
  Wait(5250);

  // viva
  Precedes(sobrevive, enxerga, busca, ataca, equilibra);

}

task movMotorEsq() {
  esq.movendo = true;
  RotateMotorExPID(M_ESQ, esq.potencia, esq.angulo, false, false, true, PID_1, PID_4, PID_4);
  esq.movendo = false;
}

task movMotorDir() {
  dir.movendo = true;
  RotateMotorExPID(M_DIR, dir.potencia, dir.angulo, false, false, true, PID_1, PID_4, PID_4);
  dir.movendo = false;
}

task moveRobo() {
  Precedes(movMotorEsq, movMotorDir); // inicia concomitantemente ambos motores
}

// IMPORTANTE: no rob?, motores est?o montados de "ponta-cabe?a", o que faz com que OnFwd e OnRev tenham comportamento invertido.
// N?o encontrado configura??o para reverter este comportamento. Definir pot?ncia negativa ? um 'workaround' para isto
void corrigeOrientacao(int potencia){
  potencia *= -1;
}

sub giraEsq(int angulo, int potencia) {
  esq.angulo = angulo;
  esq.potencia = potencia;
  esq.sentido = tras;
  dir.angulo = angulo;
  dir.potencia = potencia;
  dir.sentido = frente;
  StartTask(moveRobo);
}

sub giraDir(int angulo, int potencia) {
  dir.angulo = angulo;
  dir.potencia = potencia;
  dir.sentido = tras;
  esq.angulo = angulo;
  esq.potencia = potencia;
  esq.sentido = frente;
  StartTask(moveRobo);
}

task ataca() {
  while (true) {
    if (modo == atacando) {
      OnFwd(M_PA, 100); // liga p?
      Acquire(movMutex);
      RotateMotorEx(MOTRIZES, POTENCIA_MOTOR, 45, 0, true, false); // ataca (de 45 graus cada vez)
      Release(movMutex);
    } else {
      Off(M_PA); //desliga p?
      //OnRev(MOTORES, POTENCIA_MOTOR);
      //Wait(250); // TODO: apenas retornar se perder o ataque
    }
  }
}

task enxerga() {
  byte buffer[8];
  CommLSReadType args;
  args.Port = SENSOR_ULTRASOM;
  args.Buffer = buffer;
  args.BufferLen = 8;
  while (true) {
    if (modo != sobrevivendo){
      Wait(50);
      SysCommLSRead(args);
      if(args.Result == NO_ERR){
        // TODO: arrumar erro de compilacao
        if(/*calculaMediaLeitura(args.Buffer)*/1 < GATILHO_DISTANCIA){ 
          modo = atacando;  
          PlaySound(SOUND_UP); // tambores!!!
        }
        else {
          modo = buscando;
        }
      }
    }
  }
}

task busca(){
  while (true){
    if (modo == buscando){
      // TODO: andar girar
    }
  }
}

task equilibra(){
  while (true) {
    // verifica se perdeu contato com o solo
    if (modo != sobrevivendo && SENSOR_4 == 0){ // perda contato
      // TODO: retornar poucos cm e voltar 
    }
  }
}

task sobrevive() {
  unsigned int val;
  while(true){
    val = SensorValueRaw(SENSOR_COR); // verificar se tem que ler com S2
    if (val < GATILHO_LUZ){
      // mutex SOBREVIVENCIA
      modo = sobrevivendo;
      // TODO: foge da situacao
      modo = buscando;
    }
  }
}

float calculaMediaLeitura(byte valores[], int tamanho){
  float media = 0;
  for (int i = 0; i < tamanho; i++){
    media += valores[i];
  }
  return media / tamanho;
}